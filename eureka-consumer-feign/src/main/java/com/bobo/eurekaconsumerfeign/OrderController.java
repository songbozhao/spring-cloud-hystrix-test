package com.bobo.eurekaconsumerfeign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author bobo
 * @date 2020-12-10
 */
@RestController
public class OrderController {

    @Autowired
    private OrderInterface orderInterface;

    @RequestMapping("/order")
    public String order(){
        return orderInterface.order();
    }
}
