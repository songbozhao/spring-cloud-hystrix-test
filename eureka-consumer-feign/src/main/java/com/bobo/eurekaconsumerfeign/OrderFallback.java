package com.bobo.eurekaconsumerfeign;

import org.springframework.stereotype.Component;

/**
 * @author bobo
 * @date 2020-12-10
 */
@Component
public class OrderFallback implements OrderInterface {

    private Throwable throwable;

    public OrderFallback() {
    }

    public OrderFallback(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public String order() {
        if (throwable != null) {
            return throwable.toString();
        }
        return "很抱歉，请求订单失败！";
    }
}
