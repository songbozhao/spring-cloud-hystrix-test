package com.bobo.eurekaconsumerfeign;

import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

/**
 * @author bobo
 * @date 2020-12-10
 */
@Component
public class OrderFallbackFactory implements FallbackFactory<OrderFallback> {
    @Override
    public OrderFallback create(Throwable throwable) {
        return new OrderFallback(throwable);
    }
}
