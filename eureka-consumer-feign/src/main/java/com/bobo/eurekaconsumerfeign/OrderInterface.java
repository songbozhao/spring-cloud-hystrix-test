package com.bobo.eurekaconsumerfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author bobo
 * @date 2020-12-10
 */
//@FeignClient(name = "eureka-provider", fallback = OrderFallback.class)
@FeignClient(name = "eureka-provider", fallbackFactory = OrderFallbackFactory.class)
public interface OrderInterface {
    @RequestMapping("/getOrder")
    String order();
}
