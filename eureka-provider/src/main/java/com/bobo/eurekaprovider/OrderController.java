package com.bobo.eurekaprovider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @author bobo
 * @date 2020-12-10
 */
@RestController
public class OrderController {

    @Value("${server.port}")
    private int value;

    @RequestMapping("/getOrder")
    public String getOrder() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "我是订单，我的端口为：" + value;
    }
}
